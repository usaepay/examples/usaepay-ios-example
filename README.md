===============================

PaymentEngine Middleware iOS SDK Ver. 2.3.19

For documentation please see https://help.usaepay.info/sdk/paymentengine/middleware/#ios-middleware

==============================

==============

1. Overview

==============

The PaymentEngine Middleware library allow developers to process EMV, contactless and swiped transactions on the USAePay gateway. The library handles all communication between the hardware and the gateway. This allows developers to create rich mobile payment applications without the hassle and expense of EMV certification.

The library supports iOS 11.0+

==============

Change Log

==============

v2.3.19 - 2022-03-28

- Added BBPOS Support
- Added in more examples to demo app (Objective-c / Swift)

v2.3.18 - 2020-10-07

- Added in Cloud API support
- Updated demo to include Cloud API examples
- Updated demo to swift 5.0

v2.3.17 - 2019-06-27

- Added in the ability to automatically cancel transaction if user kills app and MP200 is stuck in processing state when app is relaunch
- Updated deprecated methods
- Added in more error handling methods
- Updated demo to swift 4.0

v2.3.16 - 2019-02-27

- Added in the ability get a list of connected bluetooth devices
- Added in the ability to connect to a specific connected MP200/V3 device
- Added in new class PaymentEngineAccessoryObject to parse connected device info
- Added in the ability to print text using Vega 3 with printer support
- Added in the ability to obtain EMV Vega 3 receipt
- Added in more examples to demo app (Objective-c / Swift)

v2.3.15 - 2018-11-28
- Added in the ability to update queued transaction
- Added in identifier field in PaymentEngineTransactionResponse
- Added in more examples to demo app

v2.3.14 - 2018-10-02
- Added in Store Forwarding
- Added in examples for store forwarding

v2.3.13 - 2018-08-27
- Added in examples on how to use PaymentEngineAPICom

v2.3.12 - 2018-07-25
- Added in setDeviceTimeout and getDeviceTimeout

v2.3.11 - 2018-05-30
- Fixed cardholder name issue coming back as Null


v2.3.10 - 2018-05-17
- Added in the ability to Manually display image
- Added in the ability to scan barcode and print text with V3

v2.3.9 - 2018-04-13
- Add in adjustment

v2.3.8 - 2018-03-08
- Added in fixes for display image when voiding transaction

v2.3.7 - 2018-12-20
- Added in bitcode support that is required by xcode 9.0

v2.3.6 - 2017-07-07
- Returning fallback as one of the transactionType
- Added in more examples to demo apps

v2.3.5 - 2017-06-08
- Added in more examples to demo apps
- Enhanced the method on getting device info
- Added in more info returned from PaymentEngineTransactionResponse class

v2.3.4 - 2017-05-11
- Added in more examples to demo apps
- Added in manual key support
- Added in Debit Boolean value to enable/disable debit selection

v2.3.3 - 2017-04-10
- Added in more examples to demo apps
- Fixed Bridging issue for Swift
- Allow developer to disable/enable cash back

v2.3.2 - 2017-03-23
- Added in Swift demo app

v2.3.1 - 2017-01-23
- Support for First Data Nashville and Tsys processing platforms


