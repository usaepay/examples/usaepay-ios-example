//
//  ViewController.swift
//  SwiftDemo
//
//  Created by jay lei on 2/17/17.
//  Copyright © 2017 Jay Lei. All rights reserved.
//

import UIKit

class ViewController: UIViewController , PaymentEngineMiddlewareDelegate
{

    @IBOutlet weak var startTransBtn: UIButton!
    @IBOutlet weak var getDeviceBtn: UIButton!
    @IBOutlet weak var getReceiptBtn: UIButton!
    @IBOutlet weak var captureSignatureBtn : UIButton!
    @IBOutlet weak var voidTransBtn : UIButton!
    @IBOutlet weak var adjustTransBtn: UIButton!
    @IBOutlet weak var testApiBtn: UIButton!
    @IBOutlet weak var startOfflineBtn: UIButton!
    @IBOutlet weak var retrieveQueueBtn: UIButton!
    @IBOutlet weak var runQueueBtn: UIButton!
    @IBOutlet weak var updateQueueBtn: UIButton!
    @IBOutlet weak var connectDeviceBtn: UIButton!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    let middlewareSetting = PaymentEngineMiddlewareSettings.getInstance()
    let middleware = PaymentEngineMiddleware.getInstance()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        middlewareSetting?.currentMode = "Production"
        middlewareSetting?.sourceKey = "Your Source Key Here"
        middlewareSetting?.pinNum = "Your Pin Here"
        
        /**
         * The below setting values are optional, by default they are set to false
         * This is used to print out the middleware debugging log during development
         */
        middlewareSetting?.enableDebugLog = true;
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateResultLog), name: NSNotification.Name("MiddlewareLogNotification"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.userFlowNotif), name: NSNotification.Name("MiddlewareFlowNotification"), object: nil)
        
      scrollView.contentInset = UIEdgeInsetsMake(20, 0.0, 444.0, 0.0)
        
        /**
         * Initialize the middleware class with the device we will be using
         */
        middleware?.setDevice("castle", self)
        
        //To connect to a specific device, use this statement to instantiate
     //   middleware?.setDevice("", self)
        
        setUpBtn(btn: startTransBtn)
        setUpBtn(btn: getDeviceBtn)
        setUpBtn(btn: getReceiptBtn)
        setUpBtn(btn: captureSignatureBtn)
        setUpBtn(btn: voidTransBtn)
        setUpBtn(btn: adjustTransBtn)
        setUpBtn(btn: testApiBtn)
        setUpBtn(btn: startOfflineBtn)
        setUpBtn(btn: retrieveQueueBtn)
        setUpBtn(btn: runQueueBtn)
        setUpBtn(btn: updateQueueBtn)
        setUpBtn(btn: connectDeviceBtn)
    
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var prefersStatusBarHidden : Bool
    {
        return true;
    }
    
    func setUpBtn (btn : UIButton)
    {
        btn.setTitleColor(.white, for: .normal);
        btn.layer.cornerRadius = 8.0;
        btn.backgroundColor = UIColor(red: 60.0/255.0, green: 114.0/255.0, blue: 153.0/255.0, alpha: 1.0);
    }
    
    /**
     * Delegate method for startTransaction, it gets call once the transaction is completed
     * @param transResponse - Holds all the response result from the gateway
     */
    func transactionComplete(_ transResponse: PaymentEngineTransactionResponse!)
    {
        
        print("status = \( (transResponse.status ?? "status is nil.")!)")
        print("result = \( (transResponse.result ?? "result is nil.")!)")
        print("error = \( (transResponse.error ?? "error is nil.")!)")
        print ("expiration = \((transResponse.expirationDate ?? "No Date Available")!)")
        print ("transaction type = \((transResponse.transactionType ?? " ")!)")
        print ("transaction cardholder name = \((transResponse.cardHolderName ?? " ")!)")
        print ("transaction identifier = \((transResponse.identifier ?? " ")!)")//This is for offline transaction
        
        let deviceAlertController = UIAlertController(title: "Status", message: transResponse.error, preferredStyle: UIAlertControllerStyle.alert);
        deviceAlertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default, handler:nil));
        
        self.present(deviceAlertController, animated: true, completion: nil);
        
    }

    /**
     * Delegate method for getReceipt , it gets called once the EMV receipt is returned from the gateway
     * @param receiptValue - Holds all the necessary data for EMV receipt
     */
    func returnReceiptCompleted (_ receiptValue : String!)
    {
        print( receiptValue!)
    }
    
    /**
      * Delegate method for readDeviceInfo,
     *  @param deviceInfoDict - Dictionary that holds information about the merchant and the features they support
   */
    func readDeviceInfoComplete(_ deviceInfoDict: [AnyHashable : Any]!) {
        
        NSLog("%@", deviceInfoDict);
    }
    
    /**
     * Delegate method for captureSignature , it gets call once we finish capturing the signature
     * @param transResponse - Holds all the response result from the gateway
     */
    func captureSignatureComplete(_ transResponse: PaymentEngineTransactionResponse!) {
        print("error = \( (transResponse.error ?? "error is nil.")!)")
        
        let deviceAlertController = UIAlertController(title: "Status", message: transResponse.error, preferredStyle: UIAlertControllerStyle.alert);
        deviceAlertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default, handler:nil));
        
        self.present(deviceAlertController, animated: true, completion: nil);
    }
    
    /**
     * Delegate method for captureSignature , it gets call once we finish capturing the signature
     * @param transResponse - Holds all the response result from the gateway
     */
    func adjustTranstionComplete(_ transResponse: PaymentEngineTransactionResponse!) {
        print("error = \( (transResponse.error ?? "error is nil.")!)")
        
        let deviceAlertController = UIAlertController(title: "Status", message: transResponse.status, preferredStyle: UIAlertControllerStyle.alert);
        deviceAlertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default, handler:nil));
        
        self.present(deviceAlertController, animated: true, completion: nil);
    }
    
    @objc func updateResultLog(notification: NSNotification)
    {
        let logString  = notification.object
        
        print( logString!)
    }
    
    @objc func userFlowNotif(notification: NSNotification)
    {
        let logString  = notification.object
        
        print( logString!)
    }

    @IBAction func startTransBtnEvent(sender: UIButton)
    {

        /**
         * Check to see if the terminal is connected
         * Alert the user if the terminal is not connected
         */
        if(middleware?.isDeviceOnline())!
        {
             //Creates an empty NSmutableDictionary objects to hold all the transaction info
            let transDict  = NSMutableDictionary()
            
            transDict.setObject("cc:sale", forKey: "command" as NSCopying)
            transDict.setObject("40.00", forKey: "amount" as NSCopying) //Total auth amount including tip
            transDict.setObject("5.00", forKey: "tip" as NSCopying)
            transDict.setObject("10000", forKey: "invoice" as NSCopying)
            transDict.setObject("This is my first middleware transaction", forKey: "description" as NSCopying)
            
            //To enable offline transaction when there is no internet connectivity
           // transDict.setObject("true", forKey: "enableOffline" as NSCopying)
            transDict.setObject("false", forKey: "forceOffline" as NSCopying)
            
            //Use the stratTransaction method from the UsaepayMiddleware class to start the transaction.
            middleware?.startTransaction(transDict)
            
        }
        
        else
        {
            let deviceAlertController = UIAlertController(title: "No Devices Connected", message: "Please connect the terminal to the device", preferredStyle: UIAlertControllerStyle.alert);
            deviceAlertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default, handler:nil));
            
            self.present(deviceAlertController, animated: true, completion: nil);
        }
    }

    @IBAction func getDeviceInfoEvent(sender: UIButton)
    {
            middleware?.readDeviceInfo()
    }
    
    @IBAction func getReceiptEvent(sender: UIButton)
    {
        /**
         * 1500188080 - It's a test Transaction ID
         * The value is obtained from PaymentEngineTransactionResponse after transaction is completed
         */
        
        let receiptDict : Dictionary = ["refnum" : "1500188080"]

        middleware?.getReceipt(receiptDict)
    }

    @IBAction func captureSignatureEvent(sender: UIButton)
    {
        let signatureImg : UIImage = UIImage(named: "SignatureImg")!
        let imgData = UIImagePNGRepresentation(signatureImg)
        let imgBase64 : String = (imgData?.base64EncodedString(options: Data.Base64EncodingOptions(rawValue: 0)))!
        
        let transDict = NSMutableDictionary()
        
        transDict.setObject(imgBase64, forKey: "signature" as NSCopying)
        
        /**
         * Reference number of the the transaction that we want to capture the signature for
         * Below is a test reference number
         */
        transDict.setObject("1537823795", forKey: "refNum" as NSCopying)
        
        /**
         * The amount of the transaction
         * This amount needs to match the reference number amount
         * If amount is different, we will get an error capturing the signature
         */
        transDict.setObject("40", forKey: "amount" as NSCopying)
        transDict.setObject("capture", forKey: "command" as NSCopying)
        
        middleware?.captureSignature(transDict)

    }
    
    @IBAction func voidTransEvent(sender : UIButton)
    {
        if(middleware?.isDeviceOnline())!
        {
            let voidAlertController = UIAlertController(title: "Void Transaction", message: "Enter transaction reference number",  preferredStyle: UIAlertControllerStyle.alert)
            voidAlertController.addTextField(configurationHandler: {(_ textfield: UITextField) -> Void in
                
                textfield.keyboardType = UIKeyboardType.numberPad
                textfield.placeholder = "Reference Number"
                
            })
            
            let okAction = UIAlertAction(title: "Ok", style:UIAlertActionStyle.default, handler: {(_ action: UIAlertAction) -> Void in
                
                let refNumString = voidAlertController.textFields?[0].text
                
                if(!(refNumString?.isEmpty)!)
                {
                    print("string not empty \(String(describing: voidAlertController.textFields?[0].text))")
                    
                    //Creates an empty NSmutableDictionary objects to hold all the transaction info
                    let transDict  = NSMutableDictionary()
                    
                    transDict.setObject("cc:void", forKey: "command" as NSCopying)
                    transDict.setObject(refNumString!, forKey: "refnum" as NSCopying)
                    
                    //Use the stratTransaction method from the UsaepayMiddleware class to start the transaction.
                    self.middleware?.startTransaction(transDict)
                    
                }
                
            })
            
            voidAlertController.addAction((UIAlertAction(title:"Cancel", style:UIAlertActionStyle.default, handler:nil)))
            voidAlertController.addAction(okAction)
            
            self.present(voidAlertController, animated: true, completion: nil);

        }
        
        else
        {
            let deviceAlertController = UIAlertController(title: "No Devices Connected", message: "Please connect the terminal to the device", preferredStyle: UIAlertControllerStyle.alert);
            deviceAlertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default, handler:nil));
            
            self.present(deviceAlertController, animated: true, completion: nil);
        }
        
    }
    
    /**
     * Delegate method adjustTranstionComplete will get called once we finish adjusting the transaction
     */
    @IBAction func adjustTransEvent(_ sender: Any) {

        let transDict = NSMutableDictionary()

        /**
         * Reference number of the the transaction that we want to adjust
         * Below is a test reference number
         */
        transDict.setObject("1919214943", forKey: "refNum" as NSCopying)
        
        //total amount including tip
        transDict.setObject("60", forKey: "amount" as NSCopying)
        transDict.setObject("10", forKey: "tip" as NSCopying)
        transDict.setObject("adjust", forKey: "command" as NSCopying)
        
        middleware?.adjustTransaction(transDict)
    }
    
    @IBAction func startOfflineTransEvent(sender: UIButton)
    {
        
        /**
         * Check to see if the terminal is connected
         * Alert the user if the terminal is not connected
         */
        if(middleware?.isDeviceOnline())!
        {
            //Creates an empty NSmutableDictionary objects to hold all the transaction info
            let transDict  = NSMutableDictionary()
            
            transDict.setObject("cc:sale", forKey: "command" as NSCopying)
            transDict.setObject("40.00", forKey: "amount" as NSCopying)
            transDict.setObject("10000", forKey: "invoice" as NSCopying)
            transDict.setObject("This is my first middleware transaction", forKey: "description" as NSCopying)
            
            //Passing offline to transtype to start offline transaction
             transDict.setObject("offline", forKey: "transtype" as NSCopying)
            
            //Use the stratTransaction method from the UsaepayMiddleware class to start the transaction.
            middleware?.startTransaction(transDict)
            
        }
            
        else
        {
            let deviceAlertController = UIAlertController(title: "No Devices Connected", message: "Please connect the terminal to the device", preferredStyle: UIAlertControllerStyle.alert);
            deviceAlertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default, handler:nil));
            
            self.present(deviceAlertController, animated: true, completion: nil);
        }
    }
    
    @IBAction func retrieveQueueTransEvent(sender: UIButton)
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyMMdd"
        
        /**
         Make sure there are queued transaction
         If there are queued transaction, retrieve all of them from the list
         */
        
        if (middleware?.retrieveQueuedTrans().count)! > 0 {
            for i in 0..<middleware!.retrieveQueuedTrans().count {
                let resultQueue = middleware!.retrieveQueuedTrans()[i] as? PaymentEngineResultQueue
                
                if let anIdentifier = resultQueue?.identifier {
                    print("identifier: \(anIdentifier)")
                }
                if let anAmount = resultQueue?.totalAmount {
                    print("request total amount: \(anAmount)")
                }
                if let aNum = resultQueue?.cardNum {
                    print("card num: \(aNum)")
                }
                if let aTime = resultQueue?.dateTime {
                    print("date: \(dateFormatter.string(from: aTime))")
                }
            }
        }
    }
    
    @IBAction func runQueueTransEvent(sender: UIButton)
    {
            /*
         If an identifier is passed in, it will run the transaction for that specific identifier
         else it will run all the queued transacitons
      */
        
         middleware!.runQueuedTrans("")
        
    }
    
    @IBAction func updateTransEvent(sender: UIButton)
    {
        
        /*
        Creates a new update dictionary
        Passing in the fields we want to update
    */
        let updateQueuedDict  = NSMutableDictionary()
        
        updateQueuedDict.setObject("8", forKey: "tip" as NSCopying)
        updateQueuedDict.setObject("first name", forKey: "billfname" as NSCopying)
        updateQueuedDict.setObject("last name", forKey: "billlname" as NSCopying)
        updateQueuedDict.setObject("62678589526", forKey: "billphone" as NSCopying)
        updateQueuedDict.setObject("update@test.com", forKey: "email" as NSCopying)
        
        /**
         Check to see if there are queued transaction
         This test example will get the first identifier, update the transaction base on the identifier
         */
        if (middleware?.retrieveQueuedTrans().count)! > 0
        {
            let resultQueue = middleware!.retrieveQueuedTrans()[0] as? PaymentEngineResultQueue
                
                if let anIdentifier = resultQueue?.identifier {
                    print("identifier: \(anIdentifier)")
                }
                if let anAmount = resultQueue?.totalAmount {
                    print("request total amount: \(anAmount)")
                }
                if let aNum = resultQueue?.cardNum {
                    print("card num: \(aNum)")
                }

            middleware!.updateQueuedTrans(updateQueuedDict, resultQueue?.identifier)
            
        }
        
    }
    
    @IBAction func connectDeviceEvent(_ sender: Any) {
        
        print("List of connected devices: \(String(describing: middleware!.getConnectedDevices()))")
        
        /**
         * Obtain a list of connected devices
         * Make sure there are devices connected before we try to allow user to connect
         */
        
        if (middleware!.getConnectedDevices()?.count )! > 0
        {
            let accessoryObject = middleware?.getConnectedDevices()[0] as? PaymentEngineAccessoryObject
        
            middleware?.connectBluetoothDevice(accessoryObject?.deviceName)
        }
        
    }
    
    
}

