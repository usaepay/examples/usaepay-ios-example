//
//  TestAPIViewController.swift
//  SwiftDemo
//
//  Created by jay lei on 8/27/18.
//  Copyright © 2018 Jay Lei. All rights reserved.
//

import UIKit

class TestAPIViewController: UIViewController, PaymentEngineAPIDelegate
{

    @IBOutlet weak var startTransBtn: UIButton!
    @IBOutlet weak var captureSignatureBtn : UIButton!
    @IBOutlet weak var voidTransBtn : UIButton!
    @IBOutlet weak var adjustTransBtn: UIButton!
     @IBOutlet weak var quickCreditTransBtn: UIButton!
    @IBOutlet weak var testApiBtn: UIButton!
    
    let middlewareSetting = PaymentEngineMiddlewareSettings.getInstance()
    let apiCom = PaymentEngineAPICom.getInstance()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        middlewareSetting?.currentMode = "Production"
        middlewareSetting?.sourceKey = "Your Source Key"
        middlewareSetting?.pinNum = "Your Source Pin"
        
        /**
         * The below setting values are optional, by default they are set to false
         * This is used to print out the middleware debugging log during development
         */
        middlewareSetting?.enableDebugLog = true;
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateResultLog), name: NSNotification.Name("MiddlewareLogNotification"), object: nil)
        
        setUpBtn(btn: startTransBtn)
        setUpBtn(btn: captureSignatureBtn)
        setUpBtn(btn: voidTransBtn)
        setUpBtn(btn: adjustTransBtn)
        setUpBtn(btn: quickCreditTransBtn)
        setUpBtn(btn: testApiBtn)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var prefersStatusBarHidden : Bool
    {
        return true;
    }
    
    func setUpBtn (btn : UIButton)
    {
        btn.setTitleColor(.white, for: .normal);
        btn.layer.cornerRadius = 8.0;
        btn.backgroundColor = UIColor(red: 60.0/255.0, green: 114.0/255.0, blue: 153.0/255.0, alpha: 1.0);
    }
    
    /**
     * Delegate method for startTransaction, it gets call once the transaction is completed
     * @param transResponse - Holds all the response result from the gateway
     */
    func transactionComplete(_ transResponse: PaymentEngineTransactionResponse!)
    {
        
        print("status = \( (transResponse.status ?? "status is nil.")!)")
        print("result = \( (transResponse.result ?? "result is nil.")!)")
        print("error = \( (transResponse.error ?? "error is nil.")!)")
        print ("expiration = \((transResponse.expirationDate ?? "No Date Available")!)")
        print ("transaction type = \((transResponse.transactionType ?? " ")!)")
        
        let deviceAlertController = UIAlertController(title: "Status", message: transResponse.status, preferredStyle: UIAlertControllerStyle.alert);
        deviceAlertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default, handler:nil));
        
        self.present(deviceAlertController, animated: true, completion: nil);
        
    }

    /**
     * Delegate method for captureSignature , it gets call once we finish capturing the signature
     * @param transResponse - Holds all the response result from the gateway
     */
    func captureSignatureComplete(_ transResponse: PaymentEngineTransactionResponse!) {
        print("error = \( (transResponse.error ?? "error is nil.")!)")
        
        let deviceAlertController = UIAlertController(title: "Status", message: transResponse.error, preferredStyle: UIAlertControllerStyle.alert);
        deviceAlertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default, handler:nil));
        
        self.present(deviceAlertController, animated: true, completion: nil);
    }
    
    @objc func updateResultLog(notification: NSNotification)
    {
        let logString  = notification.object
        
        print( logString!)
    }

    @IBAction func startTransBtnEvent(sender: UIButton)
    {
            //Creates an empty NSmutableDictionary objects to hold all the transaction info
            let transDict  = NSMutableDictionary()
            
            transDict.setObject("cc:sale", forKey: "command" as NSCopying)
            transDict.setObject("40.00", forKey: "amount" as NSCopying)
            transDict.setObject("10000", forKey: "invoice" as NSCopying)
            transDict.setObject("This is my first middleware transaction", forKey: "description" as NSCopying)
        
            /**
            * To start a manually key transaction use this command
            */
            transDict.setObject("First Last", forKey: "name" as NSCopying)
            transDict.setObject("5555444433332226", forKey: "card" as NSCopying)
            transDict.setObject("1222", forKey: "expir" as NSCopying)
        
            apiCom?.startTransaction(transDict)
    }
    
    @IBAction func captureSignatureEvent(sender: UIButton)
    {
        let signatureImg : UIImage = UIImage(named: "SignatureImg")!
        let imgData = UIImagePNGRepresentation(signatureImg)
        let imgBase64 : String = (imgData?.base64EncodedString(options: Data.Base64EncodingOptions(rawValue: 0)))!
        
        let transDict = NSMutableDictionary()
        
        transDict.setObject(imgBase64, forKey: "signature" as NSCopying)
        
        /**
         * Reference number of the the transaction that we want to capture the signature for
         * Below is a test reference number
         */
        transDict.setObject("2086526300", forKey: "refNum" as NSCopying)
        
        /**
         * The amount of the transaction
         * This amount needs to match the reference number amount
         * If amount is different, we will get an error capturing the signature
         */
        transDict.setObject("40", forKey: "amount" as NSCopying)
        transDict.setObject("capture", forKey: "command" as NSCopying)
        
        apiCom?.captureSignature(transDict)
        
    }
    
    /**
     * @delegate transactionComplete method gets called once we get a response from the gateway
     */
    @IBAction func voidTransEvent(sender : UIButton)
    {

        //Creates an empty NSmutableDictionary objects to hold all the transaction info
        let transDict  = NSMutableDictionary()
        
        transDict.setObject("cc:void", forKey: "command" as NSCopying)
        transDict.setObject("2086526300", forKey: "refNum" as NSCopying)

        apiCom?.startTransaction(transDict)
    }
    
    /**
     * @delegate transactionComplete method gets called once we get a response from the gateway
     */
    @IBAction func quickCreditTransEvent(_ sender: Any)
    {
        
        let transDict = NSMutableDictionary()
        
        /**
         * Reference number of the the transaction that we want to adjust
         * Below is a test reference number
         */
        transDict.setObject("2086482779", forKey: "refNum" as NSCopying)
        transDict.setObject("20", forKey: "amount" as NSCopying)
        transDict.setObject("cc:credit", forKey: "command" as NSCopying)
        
        apiCom?.startTransaction(transDict)
    }
    
    
    /**
     * @delegate transactionComplete method gets called once we get a response from the gateway
     */
    @IBAction func adjustTransEvent(_ sender: Any)
    {
        
        let transDict = NSMutableDictionary()
        
        /**
         * Reference number of the the transaction that we want to adjust
         * Below is a test reference number
         */
        transDict.setObject("2086482779", forKey: "refNum" as NSCopying)
        
        //total amount including tip
        transDict.setObject("70", forKey: "amount" as NSCopying)
        transDict.setObject("10", forKey: "tip" as NSCopying)
        transDict.setObject("adjust", forKey: "command" as NSCopying)
        
        apiCom?.startTransaction(transDict)
    }
    

}
