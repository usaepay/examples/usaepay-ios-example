//
//  PaymentEngineCloudDeviceList.h
//  Middleware-iOSLibrary
//
//  Created by jay lei on 5/12/20.
//  Copyright © 2020 USAePay. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PaymentEngineDeviceCapabilities.h"
#import "PaymentEngineTerminalInfo.h"
#import "PaymentEngineTerminalConfig.h"
#import "PaymentEngineDeviceSettings.h"

NS_ASSUME_NONNULL_BEGIN

@interface PaymentEngineCloudDeviceList : NSObject

@property (nonatomic, retain) NSString *type;
@property (nonatomic, retain) NSString *key;
@property (nonatomic, retain) NSString *apikeyid;
@property (nonatomic, retain) NSString *terminal_type;
@property (nonatomic, retain) NSString *status;
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *pairingCode;

@property (nonatomic, retain) PaymentEngineDeviceSettings *deviceSetting;
@property (nonatomic, retain) PaymentEngineTerminalInfo *deviceTerminalInfo;
@property (nonatomic, retain) PaymentEngineTerminalConfig *deviceTerminalConfig;
@property (nonatomic, retain) PaymentEngineDeviceCapabilities *deviceCapabilities;

@end

NS_ASSUME_NONNULL_END
