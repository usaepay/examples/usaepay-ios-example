//
//  PaymentEngineMiddleware.h
//  Middleware-iOSLibrary
//
//  Created by USAePay on 7/30/15.
//  Copyright (c) 2015 USAePay. All rights reserved.
//  Description: Use this class to communicate with the hardware devices.
// change

#import <Foundation/Foundation.h>
#import "PaymentEngineTransactionResponse.h"
#import <UIKit/UIKit.h>

@protocol PaymentEngineMiddlewareDelegate <NSObject>

@required

/*!
 @brief Delegate method for startTransaction.
 @param transResponse - Contains all the transaction response info from the gateway
 */
-(void)transactionComplete :(PaymentEngineTransactionResponse *)transResponse;

@optional

/*!
 @brief Delegate method for device connected. The method gets called when the device is connected
 @detail Not available on cloud devices
 */
-(void)deviceConnected;

/*!
 @brief Delegate method for device disconnected. The method gets called when the device is disconnected
 @detail Not available on cloud devices
 */
-(void)deviceDisconnected;

/*!
 @brief Call this delegate method when the receipt is returned from the gateway
 @param receiptValue - Contains all the necessary data for a receipt
 */
-(void)returnReceiptCompleted :(NSString *)receiptValue;

/*!
 @brief Call this delegate method when the V3 receipt is returned from the gateway
 @param receiptValue - Contains all the necessary data for V3 receipt
 @detail Only available on V3
 */
-(void)getV3ReceiptCompleted :(NSString *)receiptValue;

/*!
 @brief Delegate method, it will get call when we start to update the terminal config
 @detail Only available on MP200 and V3
 */
-(void)startUpdatingTerminal;

/*!
 @brief Delegate method, it will get call once we finish updating all the terminal config
 @detail Only available on MP200 and V3
 */
-(void)updateTerminalConfigCompleted :(NSString *)status message:(NSString *)msg;

/*!
 @brief This is a call back method when the signature is captured
 @param transResponse - Contains all the transaction information returned from the server
 */
-(void)captureSignatureComplete :(PaymentEngineTransactionResponse *)transResponse;

/*!
 @brief Delegate method for getMerchantCapabilities
 @param merchantCapDict - Dictionary that holds information about the merchant and the features they support
 @detail Only available on MP200 and V3
 */
-(void)getMerchantCapabilitiesComplete: (NSDictionary *)merchantCapDict;

/*!
 @brief Delegate method for readDeviceInfo
 @param deviceInfoDict - Dictionary that holds information about the merchant and the features they support
 */
-(void) readDeviceInfoComplete :(NSDictionary *)deviceInfoDict;

/*!
 @brief The delegate method gets called when adjust transaction is completed
 @param transResponse - Contains all the transaction information returned from the gateway
 */
-(void)adjustTranstionComplete :(PaymentEngineTransactionResponse *)transResponse;

/*!
 @brief Delegate method for print, this is an optional delegate method if user wants to see print error
 @param msg - returns the error message
 @detail Only available on V3
 */
-(void)printCompleted:(NSString *)msg;

/*!
 @brief delegate method, gets call once we obtain a timeout duration from the device
 @param seconds - returns the timeout duration in seconds
 @detail Only available on MP200 and V3
 */
-(void)getDeviceTimeoutCompleted :(int) seconds;

/*!
 @brief delegate method, gets call once runQueuedTrans is completed
 @param transResponse - Contains all the transaction response info from the gateway
 @detail Only available on MP200 and V3
 */
-(void)runQueuedTransCompleted :(PaymentEngineTransactionResponse *)transResponse;

/*!
 @description Delegate method for removeQueuedTrans
 @param status - Returns the status of the queued transaction, Success or Failed
 @param msg - Message explaining the status
 @detail Only available on MP200 and V3
 */
-(void)removeQueuedTransCompleted :(NSString *)status message:(NSString *)msg;

/*!
 @description - Delegate method for updateQueuedTrans
 @param status - Returns the status of the updated transaction, Success or Failed
 @param msg - Message explaining the status
 @detail Only available on MP200 and V3
 */
-(void)updateQueuedTransCompleted:(NSString *)status message:(NSString *)msg;

//------------------------------Cloud Delegate Methods------------------------------

/*!
    @brief Delegate method, this method gets called once device is finished registering.
    @param status - The status for registering device, success or failed
    @param msg - If device registriation failed, an error message will be returned
    @param infoDict - Contains the device info, a pairing code will be returned to use to pair the device
 */
- (void) registerDeviceCompleted :(NSString *)status errorMsg:(NSString *)msg deviceInfo :(NSDictionary *)infoDict;

/*!
   @brief Delegate method, this method gets called once deleteCloudDevice is completed
   @detail dict - Contains the status and error message
*/
-(void) deleteCloudDeviceCompleted :(NSDictionary *)dict;

/*!
   @brief Delegate method, this method gets called once updateCloudDevice is completed
   @detail dict - Contains the status and error message
*/
-(void) updateCloudDeviceCompleted :(NSDictionary *)dict;

/*!
   @brief Delegate method, this optional delegate method can be implemented to get the status of the device when we start to register the device
   @detail dict - Contains the device status
*/
-(void) getCloudDeviceStatusCompleted :(NSDictionary *)dict;

/*!
   @brief Delegate method, this method gets called once getCloudDeviceList is completed
   @detail deviceListDict - Contains a list of cloud devices
*/
-(void)getCloudDeviceListCompleted :(NSDictionary *)deviceListDict;

/*!
    @brief Delegate method, this method gets call once user enters the correct pairing code
    @param dict - Contains the device info
 */
-(void) pairCloudDeviceCompleted :(NSDictionary *)dict;

/*!
   @brief Delegate method, this method gets called once startTransaction is completed
   @param dict - Contains info on current transaction
   @detail Once startCloudTransaction is success,  getCloudPaymentStatus to get the status of the current transaction
*/
-(void) startCloudTransactionCompleted :(NSDictionary *)dict;

/*!
   @brief Delegate method, this method gets called once startCloudTransaction is completed
   @param dict - Contains the current payment status, error message and transaction flow
   @detail If tranaction is completed, delegate method transactionComplete will be called
*/
-(void) getCloudPaymentStatusCompleted :(NSDictionary *)dict;

@end

@interface PaymentEngineMiddleware : NSObject

+ (PaymentEngineMiddleware *) getInstance;

@property(nonatomic, weak) id<PaymentEngineMiddlewareDelegate> delegate;

//------------------------------Device Methods------------------------------

/*!
 @brief This method must be called before starting any transaction. Use it to connect to a device.
 @param deviceName The name of the device such as icmp or castle
 @param setDelegate Sets to self
 */
-(void)setDevice :(NSString *)deviceName :(id)setDelegate;

/*!
 @brief Use the method whenever a transaction is ready to be process
 @param holdTransInfo - A NSMutableDicitonary that holds all the transaction info
 */
-(void)startTransaction :(NSMutableDictionary *)holdTransInfo;

/*!
 @description - Manually connect to selected bluetooth device
 @param deviceName - Passing in the specific bluetooth name, name can be obtained using method getConnectedDevices
 @detail Only available on MP200 and V3
 */
-(void)connectBluetoothDevice :(NSString *)deviceName;

/*!
 @description - Retrieve a list of connected bluetooth devices
 @return - Returns a list of PaymentEngineAccessoryObject which contains all the information of the connected devices
 @detail Only available on MP200 and V3
 */
-(NSMutableArray *)getConnectedDevices;

/*!
 @brief Returns true if the device is online, false otherwise
 @detail Not available on cloud devices
 */
-(BOOL)isDeviceOnline;

/*!
 @brief Disconnect the terminal from the device
 @detail Not available on cloud devices
 */
-(void)disconnectDevice;

/*!
 @brief Connects the terminal to the device
 @detail Not available on cloud devices
 */
-(void)connectDevice;

/*!
 @brief This method takes the icmp device to go online
 @detail Only available on ICMP
 */
-(void)goOnline;

/*!
 @brief This method takes the icmp device to go offline
 @detail Only available on iCMP
 */
-(void)goOffline;

/*!
 @brief Deprecated in v2.3.5 - Returns all the information related to the device such as firmware, device model and etc...
 */
-(NSDictionary *)getDeviceInfo;

/*!
 @brief Sends a command to the device to retrieve a list of device information such as battery level, firmware, and etc..
 @delegate Callback method - readDeviceInfoComplete :(NSDictionary *)deviceInfo
 @detail Not available on cloud devices
 */
-(void)readDeviceInfo;

/*!
  @brief Cancels a transaction that is still in progress
  @detail Not available on cloud devices
 */

-(void)cancelTrans;

/*!
 @brief Call this method to start printing text
 @param text - The string value to print
 @detail only available on V3 terminal with printer capability
 */
-(void) printText: (NSString *)text;

/*!
 @brief Call this method to manually display image on the castle device
 @param imgType - a list of image enum type can be found in PaymentEngineMiddlewareSettings.h
 @param second - Image Timeout in seconds
 @detail Only available on V3 and MP200
 */
-(void)displayImg :(DisplayImageType )imgType timeOut:(int)second;

/*!
 @brief Call this method to manually set device timeout ( Only supported on MP200 and V3)
 @param second - Timeout in seconds
 @detail Only available on V3 and MP200
 */
-(void)setDeviceTimeout :(int)second;

/*!
 @brief Call this method to manually get the device timeout duration ( Only supported on MP200 and V3)
 @delegate Callback method - getDeviceTimeoutCompleted :(int) seconds
 @detail Only available on V3 and MP200
 */
-(void)getDeviceTimeout;

/*!
 @brief Call this method to manually set the Date and Time for MagTek Device
 @detail Only available for Magtek Device
 */
-(void)setMagTekDateTime;

/*!
 @brief This is a call back method, the method gets called when a card requires user to select Debit/Non-Debit
 @detail Only available for Magtek Device
 */
-(void)onApplicationSelect :(NSMutableArray *)selectionAryValues;

/*!
 @brief Call this method when user selects an application value
 @detail Only available for Magtek Device
 */
-(void)setApplicationSelectValue :(NSString *)selectedValue;

//------------------------------Cloud Methods------------------------------

/*!
@brief Call this method to register a new cloud device
@param deviceName - The name of the registered cloud device
@param type - The type of registered device, currently we support Standalone or Direct
*/
-(void) registerDevice :(NSString *)deviceName deviceType:(NSString *)type;

/*!
@brief Call this method to delete a specific cloud device
@param deviceKey - The device key of the the cloud device
@delegate Callback method - deleteCloudDeviceCompleted
*/
-(void) deleteCloudDevice :(NSString *)deviceKey;

/*!
    @brief Call this method to update a specific cloud device
    @param deviceKey - The device key of the the cloud device
    @param name - The name of the device, ( Optional: can be nil or empty )
    @param configDict - The terminal config for the cloud device, ( Optional: can be nil or empty )
    @param settingDict - The setting for the cloud device, ( Optional: can be nil or empty )
    @delegate Callback method - updateCloudDeviceCompleted
*/
-(void) updateCloudDevice :(NSString *)deviceKey deviceName:(NSString *)name terminalConfigDict :(NSDictionary *)configDict settingDict :(NSDictionary *)settingDict;

/*!
@brief Call this method to get a list of cloud devices
@delegate Callback method - getCloudDeviceListCompleted
*/
-(void) getCloudDeviceList;

/*!
    @brief Call this method to get the status of the current cloud payment device
    @param deviceKey - The device key of current connected cloud device
    @delegate Callback method - getDeviceStatusCompleted
*/
-(void) getCloudDeviceStatus :(NSString *)deviceKey;

/*!
    @brief Call this method cancel cloud transaction
    @param requestKey - The current transaction request key, obtained from startTransaction
    @delegate Callback method - transactionComplete
*/
-(void) cancelCloudPaymentTransaction :(NSString *)requestKey;

/*!
    @brief Call this method to get the status of the current cloud payment transaction
    @param requestKey - The current transaction request key, obtained from startTransaction
    @delegate Callback method - getCloudPaymentStatusCompleted
*/
-(void) getCloudPaymentStatus :(NSString *)requestKey;


//------------------------------Gateway Methods------------------------------

/*!
 @brief Use the method to capture a signature
 @param holdCaptureInfo - Contains all the transaction info and signature image in base64 encoded format
 */
-(void)captureSignature :(NSMutableDictionary *)holdCaptureInfo;

/*!
 @brief Adjust the transaction amount that is already processed, user can use this method to add tips to existing transaction or adjust the transaction amount
 @paramd holdTransInfo - A NSMutableDictionary that holds all the transaction info
 */
-(void)adjustTransaction :(NSMutableDictionary *)holdTransInfo;

/*!
 @brief Use the method to obtain receipt from gateway
 @param receiptDict - Contains all the necessary information for a receipt
 */
-(void)getReceipt :(NSDictionary *)receiptDict;

/*!
 @brief Use this method to obtain V3 receipt from gateway
 @param transKey - Key of the transaction that we want to obtain the receipt for
 @detail Only available on V3
 */
-(void)getV3Receipt :(NSString *) transKey;

/*!
 @brief Use the method to obtain merchant's capability
 @param autoUpdateConfigs - Setting it to true will automatically updates the merchant capability in the device if the device is connected
 @detail Only available on V3 and MP200
 */
-(void)getMerchantCapabilities :(BOOL)autoUpdateConfigs;

//------------------------------Store Forward Methods Only Available on MP200------------------------------

/*!
 @brief This method returns a list of PaymentEngineResultQueue objects. The object contains transaction information
 @detail Only available on MP200 and V3
 */
-(NSMutableArray *)retrieveQueuedTrans;

/*!
 @brief Call this method to run a single or all queued offline transactions
 @param identifier - A unique identifier for a queued transaction. If it's empty, it will run all the queued transaction
 @delegate Calls runQueuedTransCompleted
 @detail Only available on MP200 and V3
 */
-(void)runQueuedTrans: (NSString *)identifier;

/*!
 @brief - Remove queued transactions
 @param identifier - queued transaction identifier, if identifier is empty it will remove all queued transaction else remove specific transaction
 @delegate - Calls removeQueuedTransCompleted
 @detail Only available on MP200 and V3
 */
-(void)removeQueuedTrans:(NSString *)identifier;

/*!
 @brief - Update an existing queued transaction
 @param transInfo - Contains all the transaction that needed to be updated
 @param identifier - Unique queued transaction identifier
 @delegate - Calls updateQueuedTransCompleted
 @detail Only available on MP200 and V3
 */
-(void)updateQueuedTrans :(NSMutableDictionary *)transInfo :(NSString *)identifier;

@end
