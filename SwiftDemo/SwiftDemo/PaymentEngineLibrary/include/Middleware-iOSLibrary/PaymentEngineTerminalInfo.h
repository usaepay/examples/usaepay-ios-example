//
//  PaymentEngineTerminalInfo.h
//  Middleware-iOSLibrary
//
//  Created by jay lei on 5/12/20.
//  Copyright © 2020 USAePay. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface PaymentEngineTerminalInfo : NSObject

@property (nonatomic, retain) NSString *make;
@property (nonatomic, retain) NSString *model;
@property (nonatomic, retain) NSString *revision;
@property (nonatomic, retain) NSString *serial;
@property (nonatomic, retain) NSString *key_pin;
@property (nonatomic, retain) NSString *key_pan;

@end

NS_ASSUME_NONNULL_END
