//
//  PaymentEngineAPICom.h
//  Middleware-iOSLibrary
//
//  Created by USAePay on 6/4/18.
//  Copyright © 2018 USAePay. All rights reserved.
//
//  Description: Use this class to directly communicate with our gateway API

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "PaymentEngineTransactionResponse.h"

@protocol PaymentEngineAPIDelegate<NSObject>

@required

/*!
 @brief Delegate method for startTransaction.
 @param transResponse - Contains all the transaction response info from the gateway
 */
-(void)transactionComplete :(PaymentEngineTransactionResponse *)transResponse;

@optional

/*!
 @brief This is a call back method when the signature is captured
 @param transResponse - Contains all the transaction information returned from the server
 */
-(void)captureSignatureComplete :(PaymentEngineTransactionResponse *)transResponse;

-(void) loginCompleted :(NSDictionary *)loginDict;

@end

@interface PaymentEngineAPICom : NSObject

+ (PaymentEngineAPICom *) getInstance;

@property(nonatomic, weak) id<PaymentEngineAPIDelegate> delegate;

-(void) loginMerchant :(NSString *)userName password :(NSString *)pass;

/*!
 @brief Use the method to process, void or credit transaction. Look at our API documentation for a list of command
 @param holdTransInfo - A NSMutableDicitonary that holds all the transaction info which is required to start a single transaction
 */
-(void)startTransaction :(NSMutableDictionary *)holdTransInfo;

/*!
 @brief Use this method to capture the signature on a transaction
 @param signatureDict - A NSMutableDicitonary that holds all the info which is required to capture a signature
 */
-(void)captureSignature :(NSMutableDictionary *)signatureDict;

@end
