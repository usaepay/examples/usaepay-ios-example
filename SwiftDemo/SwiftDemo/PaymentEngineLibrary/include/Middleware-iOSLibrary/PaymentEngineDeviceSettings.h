//
//  PaymentEngineDeviceSettings.h
//  Middleware-iOSLibrary
//
//  Created by jay lei on 5/12/20.
//  Copyright © 2020 USAePay. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface PaymentEngineDeviceSettings : NSObject

@property (nonatomic) int timeOut;
@property (nonatomic) BOOL share_device;
@property (nonatomic) BOOL notify_update;
@property (nonatomic) BOOL notify_update_next;
@property (nonatomic) BOOL enable_standalone;
@property (nonatomic) int sleep_battery_device;
@property (nonatomic) int sleep_battery_display;
@property (nonatomic) int sleep_powered_device;
@property (nonatomic) int sleep_powered_display;

@end

NS_ASSUME_NONNULL_END
