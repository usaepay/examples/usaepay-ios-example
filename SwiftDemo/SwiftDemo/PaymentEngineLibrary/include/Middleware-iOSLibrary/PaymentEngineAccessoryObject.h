//
//  PaymentEngineAccessoryObject.h
//  Middleware-iOSLibrary
//
//  Created by jay lei on 2/25/19.
//  Copyright © 2019 USAePay. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface PaymentEngineAccessoryObject : NSObject

@property(nonatomic, retain) NSString *deviceName;
@property(nonatomic, retain) NSString *serialNum;
@property(nonatomic, retain) NSString *modelNum;
@property(nonatomic, retain) NSString *hardwareRevision;
@property(nonatomic, retain) NSString *manufacturer;

@end

NS_ASSUME_NONNULL_END
