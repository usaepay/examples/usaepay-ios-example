//
//  BBPOSViewController.h
//  PaymentEngineDemo
//
//  Created by jay lei on 3/10/22.
//  Copyright © 2022 Jay Lei. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PaymentEngineMiddleware.h"
#import "PaymentEngineMiddlewareSettings.h"

NS_ASSUME_NONNULL_BEGIN

@interface BBPOSViewController : UIViewController
{
    PaymentEngineMiddlewareSettings *middlewareSetting;
    PaymentEngineMiddleware *middleware;
    
    IBOutlet UIButton *startTransBtn;
    IBOutlet UIButton *connectBtn;
    IBOutlet UIButton *testHardwareBtn;
}

- (IBAction) startTransEvent:(id)sender;
- (IBAction) connectBBPosEvent:(id)sender;

@end

NS_ASSUME_NONNULL_END
