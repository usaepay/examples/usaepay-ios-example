//
//  BBPOSViewController.m
//  PaymentEngineDemo
//
//  Created by jay lei on 3/10/22.
//  Copyright © 2022 Jay Lei. All rights reserved.
//

#import "BBPOSViewController.h"

@implementation BBPOSViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    middlewareSetting = [PaymentEngineMiddlewareSettings getInstance];
    middleware = [PaymentEngineMiddleware getInstance];
    
    /**
     * The mode can be either sandbox or production
     * If developer wants to test on our sandbox server, they will set the currentMode to sandbox
     * It's not case sensitive
     */
    middlewareSetting.currentMode = @"Production";
    middlewareSetting.sourceKey = @"Your Source Key";
    middlewareSetting.pinNum = @"Your Pin";
    
    /**
     * The below setting values are optional, by default they are set to false
     * This is used to print out the middleware debugging log during development
     */
    middlewareSetting.enableDebugLog = true;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateResultLog:) name:MiddlewareLogNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userFlowNotif:) name:MiddlewareFlowNotification object:nil];


    [self setUpBtn :startTransBtn];
    [self setUpBtn :connectBtn];
    [self setUpBtn :testHardwareBtn];

}

-(BOOL)prefersStatusBarHidden
{
    return true;
}

/**
 * The delegate notification method prints out all the middleware debugging logs
 */
-(void)updateResultLog :(NSNotification *)notif
{
    
    NSString *logString = [notif object];
    
    NSLog(@"result log: %@", logString);
}


/**
 * The delegate notification method prints out the transaction flow logs
 */
-(void)userFlowNotif :(NSNotification *)notif
{
    NSString *userFlowLog = [notif object];
    
    NSLog(@"User flow log: %@", userFlowLog);

}

/*!
 @brief Delegate method for device connected. The method gets called when the device is connected
 @detail Not available on cloud devices
 */
-(void)deviceConnected
{
    NSLog(@"bbpos device connected");
}

/*!
 @brief Delegate method for device disconnected. The method gets called when the device is disconnected
 @detail Not available on cloud devices
 */
-(void)deviceDisconnected
{
    NSLog(@"bbpos device disconnected");
}

-(void)setUpBtn :(UIButton *)btn
{
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btn.layer.cornerRadius = 8.0f;
    btn.backgroundColor = [UIColor colorWithRed:60.0f/255.0f green:114.0f/255.0f blue:153.0f/255.0f alpha:1.0f];
}

/**
 * Delegate method for startTransaction, it gets call once the transaction is completed
 * @param transResponse - Holds all the response result from the gateway
 */
-(void)transactionComplete :(PaymentEngineTransactionResponse *)transResponse
{
    NSLog(@"Transaction Result: %@", transResponse.Result);
    NSLog(@"Transaction Status: %@", transResponse.Status);
    NSLog(@"Transaction Error: %@", transResponse.Error);
    
    UIAlertController *terminalAlert = [UIAlertController alertControllerWithTitle:@"Status" message:transResponse.Status preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                               {
                                   [self dismissViewControllerAnimated:YES completion:nil];
                               }];
    
    [terminalAlert addAction:okAction];
    
    [self presentViewController:terminalAlert animated:YES completion:nil];
}

- (IBAction) startTransEvent:(id)sender
{
    NSLog(@"start trans clicked");
    
    /**
       * Chip DNA start transaction format
     */
    
    NSMutableDictionary *requestDict = [[NSMutableDictionary alloc]init];

    [requestDict setObject:@"100001" forKey:@"invoice"];
    [requestDict setObject:@"Transaction Description"  forKey:@"description"];
    [requestDict setObject:@"sale:chipdna" forKey:@"command"];
    [requestDict setObject:@"100001" forKey:@"orderid"];
    
    [requestDict setObject:@"11.00" forKey:@"amount"];
    [requestDict setObject:@"bbpos" forKey:@"termtype"];
    
    NSMutableDictionary *amntDetailDict = [[NSMutableDictionary alloc]init];
    
    [amntDetailDict setObject:@"1.00" forKey:@"tax"];
    [amntDetailDict setObject:@"1.00" forKey:@"tip"];
    [amntDetailDict setObject:@"2.00" forKey:@"discount"];
    
    [requestDict setObject:amntDetailDict forKey:@"amount_detail"];
    
    [middleware startTransaction:requestDict];

}

- (IBAction) connectBBPosEvent:(id)sender
{

    NSLog(@"connect bbpos event clicked");
    
    /**
     * Initialize the middleware with the device we will be using
     */
    [middleware setDevice:@"bbpos" :self];
}


@end
