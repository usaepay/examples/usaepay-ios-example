//
//  TestAPIViewController.h
//  PaymentEngineDemo
//
//  Created by jay lei on 8/24/18.
//  Copyright © 2018 Jay Lei. All rights reserved.
//  Description: This example shows how to use the middleware to directly communicate with our gateway

#import <UIKit/UIKit.h>
#import "PaymentEngineAPICom.h"
#import "PaymentEngineMiddlewareSettings.h"

@interface TestAPIViewController : UIViewController<PaymentEngineAPIDelegate>
{
    PaymentEngineAPICom *apiCom;
    PaymentEngineMiddlewareSettings *middlewareSetting;
    
    IBOutlet UIButton *startTransBtn;
    IBOutlet UIButton *captureSignatureBtn;
    IBOutlet UIButton *voidTransBtn;
    IBOutlet UIButton *adjustTransactionBtn;
    IBOutlet UIButton *creditTransactionBtn;
    IBOutlet UIButton *testHardwareBtn;
}

- (IBAction) startTransaction:(id)sender;
- (IBAction) captureSignatureEvent:(id)sender;
- (IBAction) voidTransEvent:(id)sender;
- (IBAction) quickCreditTransEvent:(id)sender;
- (IBAction) adjustTransEvent:(id)sender;

@end
