//
//  PaymentEngineTerminalConfig.h
//  Middleware-iOSLibrary
//
//  Created by jay lei on 5/12/20.
//  Copyright © 2020 USAePay. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface PaymentEngineTerminalConfig : NSObject

@property (nonatomic) BOOL enable_emv;
@property (nonatomic) BOOL enable_debit_msr;
@property (nonatomic) BOOL tip_adjust;
@property (nonatomic) BOOL enable_contactless;

@end

NS_ASSUME_NONNULL_END
