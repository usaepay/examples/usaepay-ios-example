//
//  PaymentEngineDeviceCapabilities.h
//  Middleware-iOSLibrary
//
//  Created by jay lei on 5/12/20.
//  Copyright © 2020 USAePay. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface PaymentEngineDeviceCapabilities : NSObject

@property (nonatomic) BOOL emv;
@property (nonatomic) BOOL swipe;
@property (nonatomic) BOOL contactless;
@property (nonatomic) BOOL signature_capture;
@property (nonatomic) BOOL printer;
@property (nonatomic) BOOL notify_update;
@property (nonatomic) BOOL pin;
@property (nonatomic) BOOL sleep_battery;
@property (nonatomic) BOOL sleep_powered;
@property (nonatomic) BOOL standalone;

@end

NS_ASSUME_NONNULL_END
