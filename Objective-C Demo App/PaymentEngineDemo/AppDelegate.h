//
//  AppDelegate.h
//  PaymentEngineDemo
//
//  Created by USAePay on 1/17/17.
//  Copyright © 2017 USAePay. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

