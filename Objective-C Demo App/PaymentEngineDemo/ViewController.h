//
//  ViewController.h
//  PaymentEngineDemo
//
//  Created by jay lei on 1/17/17.
//  Copyright © 2017 Jay Lei. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PaymentEngineMiddleware.h"
#import "PaymentEngineMiddlewareSettings.h"

@interface ViewController : UIViewController<PaymentEngineMiddlewareDelegate>
{
    PaymentEngineMiddlewareSettings *middlewareSetting;
    PaymentEngineMiddleware *middleware;
    
    IBOutlet UIButton *startTransBtn;
    IBOutlet UIButton *getDeviceBtn;
    IBOutlet UIButton *getReceiptBtn;
    IBOutlet UIButton *captureSignatureBtn;
    IBOutlet UIButton *voidTransBtn;
    IBOutlet UIButton *adjustTransactionBtn;
    IBOutlet UIButton *testApiBtn;
    IBOutlet UIButton *startOfflineBtn;
    IBOutlet UIButton *retrieveQueueBtn;
    IBOutlet UIButton *runQueueBtn;
    IBOutlet UIButton *updateQueueBtn;
    IBOutlet UIButton *connectDeviceBtn;
    IBOutlet UIButton *testBBPOSBtn;
    
    IBOutlet UIScrollView *scrollView;
}

- (IBAction) startTransaction:(id)sender;
- (IBAction) getDeviceInfoEvent:(id)sender;
- (IBAction) getReceiptEvent:(id)sender;
- (IBAction) captureSignatureEvent:(id)sender;
- (IBAction) voidTransEvent:(id)sender;
- (IBAction) adjustTransEvent:(id)sender;
- (IBAction) updateTransEvent:(id)sender;
- (IBAction)connectDeviceEvent:(id)sender;

@end

