//
//  TestAPIViewController.m
//  PaymentEngineDemo
//
//  Created by jay lei on 8/24/18.
//  Copyright © 2018 Jay Lei. All rights reserved.
//

#import "TestAPIViewController.h"

@implementation TestAPIViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    middlewareSetting = [PaymentEngineMiddlewareSettings getInstance];
    apiCom = [PaymentEngineAPICom getInstance];
    apiCom.delegate = self;
    
    /**
     * The mode can be either sandbox or production
     * If developer wants to test on our sandbox server, they will set the currentMode to sandbox
     * It's not case sensitive
     */
    middlewareSetting.currentMode = @"Production";
    middlewareSetting.sourceKey = @"Your Source Key Here";
    middlewareSetting.pinNum = @"Your Pin Here";
    
    /**
     * The below setting values are optional, by default they are set to false
     * This is used to print out the middleware debugging log during development
     */
    middlewareSetting.enableDebugLog = true;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateResultLog:) name:MiddlewareLogNotification object:nil];

    [self setUpBtn :startTransBtn];
    [self setUpBtn :captureSignatureBtn];
    [self setUpBtn :voidTransBtn];
    [self setUpBtn :adjustTransactionBtn];
    [self setUpBtn :creditTransactionBtn];
    [self setUpBtn :testHardwareBtn];


}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)prefersStatusBarHidden
{
    return true;
}

-(void)setUpBtn :(UIButton *)btn
{
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btn.layer.cornerRadius = 8.0f;
    btn.backgroundColor = [UIColor colorWithRed:60.0f/255.0f green:114.0f/255.0f blue:153.0f/255.0f alpha:1.0f];
}

/**
 * @delegate transactionComplete method gets called once we get a response from the gateway
 */
- (IBAction)startTransaction:(id)sender
{
        //Creates an empty dictionary objects to hold all the transaction info
        NSMutableDictionary *transDict = [NSMutableDictionary new];
        
        /**
         * Sets the basic info that is needed to start a transaction
         */
        [transDict setObject:@"cc:sale" forKey:@"command"];
        [transDict setObject:@"60.00" forKey:@"amount"];
        [transDict setObject:@"10000" forKey:@"invoice"];
        [transDict setObject:@"This is my first transaction" forKey:@"description"];
    
    /**
        * To start a manually key transaction use this command
        */
        [transDict setObject:@"First Last" forKey:@"name"];
        [transDict setObject:@"5555444433332226" forKey:@"card"];
        [transDict setObject:@"1222" forKey:@"expir"];
    
    /**
     * Use this if user is swiping from a credit card. Passing in the encrypted credit card information with enc:// added to the front
     */
    /* [transDict setObject:@"enc://AvYAgB9DKACDgyUqNTQyOCoqKioqKioqMDc0Ml5ZT1UvQSBHSUZUIEZPUl4yMTA2KioqKioqKioqKioqKioqKioqKioqKioqKioqPyo7NTQyOCoqKioqKioqMDc0Mj0yMTA2KioqKioqKioqKioqKioqKj8qHRwh/GlkMYKAKP3AqH/EkvsZXdgiRZZTk8srZRse5FYwxb36iNK03iRNfdTeFmlUfI969K94fo+RSL1JUKSU0ZOCUzfvpgA+inbrmZ1Tkj4ZJnY8rVjPziNcgt5kBmB4DGkXpXTrPMLPo26Z1++uylQxMzIzMDMyNzNimUkJRgEtQATqhf8D" forKey:@"magstripe"];*/
    
        //Use the startTransaction method from the PaymentEngineAPICom class to start the transaction.
        [apiCom startTransaction:transDict];

}

/**
 * Delegate method for startTransaction, it gets call once the transaction is completed
 * @param transResponse - Holds all the response result from the gateway
 */
-(void)transactionComplete :(PaymentEngineTransactionResponse *)transResponse
{
    NSLog(@"Transaction Result: %@", transResponse.Result);
    NSLog(@"Transaction Status: %@", transResponse.Status);
    NSLog(@"Transaction Error: %@", transResponse.Error);
    NSLog(@"Expiration: %@", transResponse.expirationDate);
    NSLog(@"Transaction Type: %@", transResponse.transactionType);
    NSLog(@"transaction cardholder name: %@", transResponse.cardHolderName);
    
    UIAlertController *terminalAlert = [UIAlertController alertControllerWithTitle:@"Status" message:transResponse.Status preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                               {
                           
                               }];
    
    [terminalAlert addAction:okAction];
    
    [self presentViewController:terminalAlert animated:YES completion:nil];
}

/**
 * Delegate method for captureSignature , it gets call once we finish capturing the signature
 * @param transResponse - Holds all the response result from the gateway
 */
-(void)captureSignatureComplete :(PaymentEngineTransactionResponse *)transResponse
{
    
    NSLog(@"trans resposne: %@", transResponse.Result);
    NSLog(@"trans response status: %@", transResponse.Status);
    NSLog(@"trans response error: %@", transResponse.Error);
    
    if(transResponse != nil)
    {
        NSString *status = transResponse.Error;
        
        if(status == nil || [status isEqualToString:@""])
        {
            status = transResponse.Status;
        }
        
        UIAlertController *terminalAlert = [UIAlertController alertControllerWithTitle:@"Status" message:status preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                   {
                         
                                   }];
        
        [terminalAlert addAction:okAction];
        
        [self presentViewController:terminalAlert animated:YES completion:nil];
    }
    
}

/**
 * The delegate notification method prints out all the middleware debugging logs
 */
-(void)updateResultLog :(NSNotification *)notif
{
    
    NSString *logString = [notif object];
    
    NSLog(@"result log: %@", logString);
}

/**
 * @delegate captureSignatureComplete method gets called once we get a response from the gateway
 */
- (IBAction)  captureSignatureEvent:(id)sender
{
    UIImage *signatureImg = [UIImage imageNamed:@"SignatureImg.png"];
    NSData *imgData = UIImageJPEGRepresentation(signatureImg, 0.5f);
    NSString *imgBase64 = [imgData base64EncodedStringWithOptions:0];
    
    NSMutableDictionary *transDict = [NSMutableDictionary new];
    
    [transDict setObject:imgBase64 forKey:@"signature"];
    
    /**
     * Reference number of the the transaction that we want to capture the signature for
     * Below is a test reference number
     */
    [transDict setObject:@"2083314090" forKey:@"refNum"];
    
    /**
     * The amount of the transaction
     * This amount needs to match the reference number amount
     * If amount is different, we will get an error capturing the signature
     */
    [transDict setObject:@"60" forKey:@"amount"];
    [transDict setObject:@"capture" forKey:@"command"];
    
    [apiCom captureSignature:transDict];
}

/**
 * @delegate transactionComplete method gets called once we get a response from the gateway
 */
- (IBAction)voidTransEvent:(id)sender {
    
    UIAlertController *voidAlert = [UIAlertController alertControllerWithTitle:@"Void Transaction" message:@"Enter transaction reference number" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                               {
                                   UITextField *alertTxtField = voidAlert.textFields.firstObject;
                                   
                                   if(alertTxtField.text.length > 0)
                                   {

                                       NSMutableDictionary *transDict = [NSMutableDictionary new];
                                    
                                       [transDict setObject:@"cc:void" forKey:@"command"];
                                       [transDict setObject:alertTxtField.text forKey:@"refNum"];
                                       
                                       [self->apiCom startTransaction:transDict];

                                   }
                               }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel
                                                         handler: nil];
    
    [voidAlert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        
        textField.keyboardType = UIKeyboardTypeNumberPad;
        textField.placeholder = @"Reference Number";
        
    }];
    
    [voidAlert addAction:okAction];
    [voidAlert addAction:cancelAction];
    
    [self presentViewController:voidAlert animated:YES completion:nil];
    
}

/**
 * @delegate transactionComplete method gets called once we get a response from the gateway
 */
- (IBAction) quickCreditTransEvent:(id)sender
{
    UIAlertController *voidAlert = [UIAlertController alertControllerWithTitle:@"Quick Credit Transaction" message:@"Enter transaction reference number" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                               {
                                   UITextField *alertTxtField = voidAlert.textFields.firstObject;
                                   
                                   if(alertTxtField.text.length > 0)
                                   {
                                       /**
                                           * Reference number and amount is needed to do a quick credit
                                            */
                                       NSMutableDictionary *transDict = [NSMutableDictionary new];
                                       
                                       [transDict setObject:@"cc:credit" forKey:@"command"];
                                        [transDict setObject:@"20" forKey:@"amount"];
                                       [transDict setObject:alertTxtField.text forKey:@"refNum"];
                                       
                                       [self->apiCom startTransaction:transDict];
                                       
                                   }
                               }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel
                                                         handler: nil];
    
    [voidAlert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        
        textField.keyboardType = UIKeyboardTypeNumberPad;
        textField.placeholder = @"Reference Number";
        
    }];
    
    [voidAlert addAction:okAction];
    [voidAlert addAction:cancelAction];
    
    [self presentViewController:voidAlert animated:YES completion:nil];
}

/**
 * @delegate transactionComplete method gets called once we get a response from the gateway
 */
- (IBAction) adjustTransEvent:(id)sender
{
    
    UIAlertController *quickCreditAlert = [UIAlertController alertControllerWithTitle:@"Adjust Transaction" message:@"Enter transaction reference number" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                               {
                                   UITextField *alertTxtField = quickCreditAlert.textFields.firstObject;
                                   
                                   if(alertTxtField.text.length > 0)
                                   {
                                       /**
                                            * Reference number and amount is needed to adjust the transaction
                                          */
                                       NSMutableDictionary *transDict = [NSMutableDictionary new];
                                       
                                       // Enter total amount that is included with the tip
                                       [transDict setObject:@"50" forKey:@"amount"];
                                       [transDict setObject:@"10" forKey:@"tip"];
                                       
                                       [transDict setObject:@"adjust" forKey:@"command"];
                                       [transDict setObject:alertTxtField.text forKey:@"refNum"];
                                       
                                       [self->apiCom startTransaction:transDict];
                                       
                                   }
                               }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel
                                                         handler: nil];
    
    [quickCreditAlert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        
        textField.keyboardType = UIKeyboardTypeNumberPad;
        textField.placeholder = @"Reference Number";
        
    }];
    
    [quickCreditAlert addAction:okAction];
    [quickCreditAlert addAction:cancelAction];
    
    [self presentViewController:quickCreditAlert animated:YES completion:nil];
}

@end
