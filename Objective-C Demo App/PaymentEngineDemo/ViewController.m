//
//  ViewController.m
//  PaymentEngineDemo
//
//  Created by jay lei on 1/17/17.
//  Copyright © 2017 Jay Lei. All rights reserved.
//

#import "ViewController.h"
#import "PaymentEngineAccessoryObject.h" // Used to parse bluetooth object
#import "PaymentEngineResultQueue.h"

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   
    middlewareSetting = [PaymentEngineMiddlewareSettings getInstance];
    middleware = [PaymentEngineMiddleware getInstance];

    /**
     * The mode can be either sandbox or production
     * If developer wants to test on our sandbox server, they will set the currentMode to sandbox
     * It's not case sensitive
     */
    middlewareSetting.currentMode = @"Production";
    middlewareSetting.sourceKey = @"Your Source Key Here";
    middlewareSetting.pinNum = @"Your Pin Here";
    
    /**
     * The below setting values are optional, by default they are set to false
     * This is used to print out the middleware debugging log during development
     */
    middlewareSetting.enableDebugLog = true;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateResultLog:) name:MiddlewareLogNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userFlowNotif:) name:MiddlewareFlowNotification object:nil];

    scrollView.contentSize=CGSizeMake(self.view.frame.size.width,self.view.frame.size.height + 900);
    scrollView.contentInset=UIEdgeInsetsMake(20.0,0.0, 444.0,0.0);

    /**
     * Initialize the middleware with the device we will be using
     */
   // [middleware setDevice:@"castle" :self]; // Uncomment to connect to castle device
    
    //To connect to a specific device, use this statement to instantiate
   // [middleware setDevice:@"" :self];

    [self setUpBtn :startTransBtn];
    [self setUpBtn :getDeviceBtn];
    [self setUpBtn :getReceiptBtn];
    [self setUpBtn :captureSignatureBtn];
    [self setUpBtn :voidTransBtn];
    [self setUpBtn :adjustTransactionBtn];
    [self setUpBtn:testApiBtn];
    [self setUpBtn:startOfflineBtn];
    [self setUpBtn:retrieveQueueBtn];
    [self setUpBtn:runQueueBtn];
    [self setUpBtn:updateQueueBtn];
    [self setUpBtn:connectDeviceBtn];
    [self setUpBtn:testBBPOSBtn];
    
}

/*!
 @brief Delegate method, it will get call once we finish updating all the terminal config
 */
-(void)updateTerminalConfigComplete
{
    NSLog(@"update terminal config completed");
}

/*!
 @brief Delegate method for getMerchantCapabilities
 @param merchantCapDict - Dictionary that holds information about the merchant and the features they support
 */
-(void)getMerchantCapabilitiesComplete: (NSDictionary *)merchantCapDict
{
    NSLog(@"get merchan cap completed");
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)prefersStatusBarHidden
{
    return true;
}

-(void)setUpBtn :(UIButton *)btn
{
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btn.layer.cornerRadius = 8.0f;
    btn.backgroundColor = [UIColor colorWithRed:60.0f/255.0f green:114.0f/255.0f blue:153.0f/255.0f alpha:1.0f];
}

-(void)readDeviceInfoComplete:(NSDictionary *)deviceInfoDict
{
    NSLog(@"read device completed: %@", deviceInfoDict);
}

/**
 * Delegate method for startTransaction, it gets call once the transaction is completed
 * @param transResponse - Holds all the response result from the gateway
 */
-(void)transactionComplete :(PaymentEngineTransactionResponse *)transResponse
{
    NSLog(@"Transaction Result: %@", transResponse.Result);
    NSLog(@"Transaction Status: %@", transResponse.Status);
    NSLog(@"Transaction Error: %@", transResponse.Error);
    NSLog(@"Expiration: %@", transResponse.expirationDate);
    NSLog(@"Transaction Type: %@", transResponse.transactionType);
    NSLog(@"transaction cardholder name: %@", transResponse.cardHolderName);
    NSLog(@"transaction identifier: %@", transResponse.identifier); //This is for offline transaction
    
    UIAlertController *terminalAlert = [UIAlertController alertControllerWithTitle:@"Status" message:transResponse.Status preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                               {
                                   [self dismissViewControllerAnimated:YES completion:nil];
                               }];
    
    [terminalAlert addAction:okAction];
    
    [self presentViewController:terminalAlert animated:YES completion:nil];
}

/**
 * Delegate method for getReceipt , it gets called once the EMV receipt is returned from the gateway
 * @param receiptValue - Holds all the necessary data for EMV receipt
 */
-(void)returnReceiptCompleted :(NSString *)receiptValue
{
    NSLog(@"EMV receipt value: %@", receiptValue);
}

/**
 * Delegate method for captureSignature , it gets call once we finish capturing the signature
 * @param transResponse - Holds all the response result from the gateway
 */
-(void)captureSignatureComplete :(PaymentEngineTransactionResponse *)transResponse
{

    if(transResponse != nil)
    {

        UIAlertController *terminalAlert = [UIAlertController alertControllerWithTitle:@"Status" message:transResponse.Error preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                   {
                                       [self dismissViewControllerAnimated:YES completion:nil];
                                   }];
        
        [terminalAlert addAction:okAction];
        
        [self presentViewController:terminalAlert animated:YES completion:nil];
    }
    
}


-(void)adjustTranstionComplete:(PaymentEngineTransactionResponse *)transResponse
{
    if(transResponse != nil)
    {
        
        UIAlertController *terminalAlert = [UIAlertController alertControllerWithTitle:@"Status" message:transResponse.Status preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                   {
                                       [self dismissViewControllerAnimated:YES completion:nil];
                                   }];
        
        [terminalAlert addAction:okAction];
        
        [self presentViewController:terminalAlert animated:YES completion:nil];
    }
}

/**
 * The delegate notification method prints out all the middleware debugging logs
 */
-(void)updateResultLog :(NSNotification *)notif
{
    
    NSString *logString = [notif object];
    
    NSLog(@"result log: %@", logString);

}

/**
 * The delegate notification method prints out the transaction flow logs
 */
-(void)userFlowNotif :(NSNotification *)notif
{
    NSString *userFlowLog = [notif object];
    
    NSLog(@"User flow log: %@", userFlowLog);

}

/**
 * Button Events
 */

- (IBAction)startTransaction:(id)sender
{
    /**
     * Check to see if the terminal is connected
     * Alert the user if the terminal is not connected
     */
    if([middleware isDeviceOnline])
    {
        //Creates an empty dictionary objects to hold all the transaction info
        NSMutableDictionary *transDict = [NSMutableDictionary new];
        
        /**
         * Sets the basic info that is needed to start a transaction
         */
        [transDict setObject:@"cc:sale" forKey:@"command"];
        [transDict setObject:@"40.00" forKey:@"amount"]; //Total auth amount including tip
        [transDict setObject:@"5.00" forKey:@"tip"];
        [transDict setObject:@"10000" forKey:@"invoice"];
        [transDict setObject:@"This is my first middleware transaction" forKey:@"description"];
        
        //To enable offline transaction when there is no internet connectivity
        // [transDict setObject:@"true" forKey:@"enableOffline"];
        
        //Use the startTransaction method from the UsaepayMiddleware class to start the transaction.
        [middleware startTransaction:transDict];
    }
    
    else
    {
        
        UIAlertController *terminalAlert = [UIAlertController alertControllerWithTitle:@"No Device Connected" message:@"Please connect the terminal to the device" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                   {
                                       [self dismissViewControllerAnimated:YES completion:nil];
                                   }];
        
        [terminalAlert addAction:okAction];
        
        [self presentViewController:terminalAlert animated:YES completion:nil];
        
    }
}

-(IBAction)getDeviceInfoEvent:(id)sender
{
    [middleware readDeviceInfo];
}

- (IBAction)  getReceiptEvent:(id)sender
{
    /**
     * 1495713081 - It's a test Transaction ID
     * The value is obtained from PaymentEngineTransactionResponse after transaction is completed
     */
    NSDictionary *receiptDict = [NSDictionary dictionaryWithObjectsAndKeys:@"1495713081", @"refnum",  nil];

    [middleware getReceipt:receiptDict];
}

- (IBAction)  captureSignatureEvent:(id)sender
{
    UIImage *signatureImg = [UIImage imageNamed:@"SignatureImg.png"];
    NSData *imgData = UIImageJPEGRepresentation(signatureImg, 0.5f);
    NSString *imgBase64 = [imgData base64EncodedStringWithOptions:0];
    
    NSMutableDictionary *transDict = [NSMutableDictionary new];
    
    [transDict setObject:imgBase64 forKey:@"signature"];
    
    /**
     * Reference number of the the transaction that we want to capture the signature for
     * Below is a test reference number
     */
    [transDict setObject:@"1537411846" forKey:@"refNum"];

    /**
     * The amount of the transaction
     * This amount needs to match the reference number amount
     * If amount is different, we will get an error capturing the signature
     */
    [transDict setObject:@"40" forKey:@"amount"];
    [transDict setObject:@"capture" forKey:@"command"];
    
    [middleware captureSignature:transDict];
}

- (IBAction)voidTransEvent:(id)sender {
    
   UIAlertController *terminalAlert = [UIAlertController alertControllerWithTitle:@"Void Transaction" message:@"Enter transaction reference number" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                               {
                                   UITextField *alertTxtField = terminalAlert.textFields.firstObject;
                                   
                                   if(alertTxtField.text.length > 0)
                                   {
                                       [self dismissViewControllerAnimated:YES completion:nil];
                                       
                                       NSMutableDictionary *transDict = [NSMutableDictionary new];
                                       
                                       [transDict setObject:@"cc:void" forKey:@"command"];
                                       [transDict setObject:alertTxtField.text forKey:@"refnum"];
                                       
                                       [self->middleware startTransaction:transDict];
                                   }
                               }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel
                                                   handler: nil];
    
    [terminalAlert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        
        textField.keyboardType = UIKeyboardTypeNumberPad;
        textField.placeholder = @"Reference Number";
        
    }];
    
    [terminalAlert addAction:okAction];
    [terminalAlert addAction:cancelAction];
    
    [self presentViewController:terminalAlert animated:YES completion:nil];
    
}

/**
 * Delegate method adjustTranstionComplete will get called once we finish adjusting the transaction
 */
- (IBAction) adjustTransEvent:(id)sender
{

    UIAlertController *terminalAlert = [UIAlertController alertControllerWithTitle:@"Void Transaction" message:@"Enter transaction reference number" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                               {
                                   UITextField *alertTxtField = terminalAlert.textFields.firstObject;
                                   
                                   if(alertTxtField.text.length > 0)
                                   {
                                       [self dismissViewControllerAnimated:YES completion:nil];
                                       
                                       NSMutableDictionary *transDict = [NSMutableDictionary new];
                                       
                                       [transDict setObject:@"adjust" forKey:@"command"];
                                       [transDict setObject:alertTxtField.text forKey:@"refnum"];
                                       
                                       [self->middleware startTransaction:transDict];
                                   }
                               }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel
                                                         handler: nil];
    
    [terminalAlert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        
        textField.keyboardType = UIKeyboardTypeNumberPad;
        textField.placeholder = @"Reference Number";
        
    }];
    
    [terminalAlert addAction:okAction];
    [terminalAlert addAction:cancelAction];
    
    [self presentViewController:terminalAlert animated:YES completion:nil];
    
    NSMutableDictionary *transDict = [NSMutableDictionary new];
    
    /**
     * Reference number of the the transaction that we want to adjust
     * Below is a test reference number
     */
    [transDict setObject:@"1919214943" forKey:@"refNum"];
    
    //total amount including tip
    [transDict setObject:@"50" forKey:@"amount"];
    [transDict setObject:@"10" forKey:@"tip"];
    [transDict setObject:@"adjust" forKey:@"command"];

    [middleware adjustTransaction:transDict];
}

- (IBAction)startOfflineTransEvent:(id)sender
{
    /**
     * Check to see if the terminal is connected
     * Alert the user if the terminal is not connected
     */
    if([middleware isDeviceOnline])
    {
        //Creates an empty dictionary objects to hold all the transaction info
        NSMutableDictionary *transDict = [NSMutableDictionary new];
        
        /**
         * Sets the basic info that is needed to start a transaction
         */
        [transDict setObject:@"cc:sale" forKey:@"command"];
        [transDict setObject:@"60.00" forKey:@"amount"];
        [transDict setObject:@"10000" forKey:@"invoice"];
        [transDict setObject:@"This is my first middleware transaction" forKey:@"description"];
        
        //Passing offline to transtype to start offline transaction
        [transDict setObject:@"offline" forKey:@"transtype"];
        
        //Use the startTransaction method from the UsaepayMiddleware class to start the transaction.
        [middleware startTransaction:transDict];
    }
    
    else
    {
        
        UIAlertController *terminalAlert = [UIAlertController alertControllerWithTitle:@"No Device Connected" message:@"Please connect the terminal to the device" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                   {
                                       [self dismissViewControllerAnimated:YES completion:nil];
                                   }];
        
        [terminalAlert addAction:okAction];
        
        [self presentViewController:terminalAlert animated:YES completion:nil];
        
    }
}

- (IBAction)retrieveQueueTransEvent:(id)sender
{

    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyMMdd"];

    NSMutableArray *retrieveTransArray = [[NSMutableArray alloc]initWithArray:middleware.retrieveQueuedTrans];
    
    /**
        Make sure there are queued transaction
        If there are queued transaction, retrieve all of them from the list
     */
    if(retrieveTransArray.count > 0)
    {
        for(int i = 0; i < [middleware retrieveQueuedTrans].count; i++)
        {
            PaymentEngineResultQueue *resultQueue = (PaymentEngineResultQueue *)[[middleware retrieveQueuedTrans] objectAtIndex:i];
            
            NSLog(@"identifier: %@", resultQueue.identifier);
            NSLog(@"request total amount: %f", resultQueue.totalAmount);
            NSLog(@"card num: %@", resultQueue.cardNum);
            NSLog(@"date: %@", [dateFormatter stringFromDate:resultQueue.dateTime]);
        }
        
    }

}

- (IBAction)runQueueTransEvent:(id)sender
{
    /*
    If an identifier is passed in, it will run the transaction for that specific identifier
    else it will run all the queued transacitons
   */
    [middleware runQueuedTrans:@""];
}

- (IBAction ) updateTransEvent:(id)sender
{

    /*
     Creates a new update dictionary
     Passing in the fields we want to update
   */
    NSMutableDictionary *updateQueuedDict = [[NSMutableDictionary alloc]init];

    [updateQueuedDict setObject:@"8" forKey:@"tip"];
    [updateQueuedDict setObject:@"first name" forKey:@"billfname"];
    [updateQueuedDict setObject:@"last name" forKey:@"billlname"];
    [updateQueuedDict setObject:@"62678589526" forKey:@"billphone"];
    [updateQueuedDict setObject:@"update@test.com" forKey:@"email"];
    
    /**
      Check to see if there are queued transaction
      This test example will get the first identifier, update the transaction base on the identifier
     */
    if([middleware retrieveQueuedTrans].count > 0)
    {
        PaymentEngineResultQueue *resultQueue = (PaymentEngineResultQueue *)[[middleware retrieveQueuedTrans] objectAtIndex:0];
        
        NSLog(@"identifier: %@", resultQueue.identifier);
        NSLog(@"request total amount: %f", resultQueue.totalAmount);
        NSLog(@"card num: %@", resultQueue.cardNum);
        
        [middleware updateQueuedTrans:updateQueuedDict :resultQueue.identifier];
    }
        
}

- (IBAction)connectDeviceEvent:(id)sender
{
    NSLog(@"List of connected devices: %@", [middleware getConnectedDevices]);
    
    /**
     * Obtain a list of conntected devices
     * Make sure there are devices connected before we try to allow user to connect
     */
    if([[middleware getConnectedDevices] count] > 0)
    {
        PaymentEngineAccessoryObject *accessoryObject = [[middleware getConnectedDevices] objectAtIndex:0];
    
        [middleware connectBluetoothDevice:accessoryObject.deviceName];
    }
}


@end
