//
//  Constants.h
//  iOSMiddleware
//
//  Created by PaymentEngine on 7/14/15.
//  Copyright (c) 2015 PaymentEngine. All rights reserved.


#import <Foundation/Foundation.h>

extern NSString * const MiddlewareLogNotification;
extern NSString * const MiddlewareFlowNotification;
extern NSString * const MiddlewareDeviceFlowNotification;

static const int CONTACTLESSPAYMENT = 200001;
static const int SWIPEDCARDPAYMENT = 200002;
static const int EMVDIPPAYMENT = 200003;
static const int MIDDLEWAREMANUALKEY = 200004;

typedef enum
{
    PIN_ONLINE,
    PIN_OFFLINE_ENCR,
    PIN_OFFLINE_PLAIN,
    PIN_OFFLINE_AND_SIGNATURE,
    SIGNATURE_REQ,
    FAIL_CVM,
    NO_CVM_REQ,
    NO_CVM_AVAIL,
    PLAIN_AND_SIGNATURE
    
}ReturnTransactionType; 

typedef enum
{
    MSR_SWIPE_TRANS,
    NFC_TRANS,
    NFC_TRANS_ICC,
    MANUALLY_KEYED,
    EMV_TRANS
    
}StartTransType;

typedef enum
{
    IdleImage = 0,
    ProcessingImage = 1,
    TransApprovedImage = 2,
    TransDeclinedImage = 3,
    TransErrorImage = 4,
    SwipeCardImage = 5
    
} DisplayImageType;

typedef enum
{
    TRANS_CANCELLED,
    TRANS_SUCCESS,
    TRANS_FAILED,
    TRANS_TIMEOUT,
    TRANS_ERROR
}ReturnResponseStatus;

@interface PaymentEngineMiddlewareSettings : NSObject

+(PaymentEngineMiddlewareSettings *)getInstance;

/*! @brief Optional value,  use to set the mode (Production or Sandbox) during device initialization. The default mode is Production. */
@property(nonatomic, retain) NSString *currentMode;

/*! @brief Optional value, use to disable/enable EMV transaction. */
@property(nonatomic) BOOL isEMVEnable;

/*! @brief Optional value, use to disable/enable MSR transaction. */
@property(nonatomic) BOOL isMSREnable;

/*! @brief Optional value, use to disable/enable contactless transaction. */
@property(nonatomic) BOOL isContactlessEnable;

/*! @brief Optional value(Not Supported by MP200), use to disable/enable tips on the device. */
@property(nonatomic) BOOL isTipEnable;

/*! @brief Optional value, use to disable/enable manually key on the mp200. */
@property(nonatomic) BOOL isManuallyKeyEnable;

/*! @brief Optional value, use to disable/enable support for Tip-Adjust on the terminal. */
@property(nonatomic) BOOL isTipAdjustEnable;

/*! @brief Optional value, use to disable/enable cashback on the terminal. */
@property(nonatomic) BOOL isCashbackEnable;

/*! @brief Optional value, use to disable/enable debit selection on the terminal. */
@property(nonatomic) BOOL isDebitEnable;

/*! @brief Boolean value returned from getMerchantCapability. Merchant account supports EMV. */
@property(nonatomic) BOOL isEMVSupported;

/*! @brief Boolean value returned from getMerchantCapability. Merchant account supports tip-adjust.  */
@property(nonatomic) BOOL isTipAdjustSupported;

/*! @brief Boolean value returned from getMerchantCapability. Merchant account supports debit MSR.  */
@property(nonatomic) BOOL isDebitMSRSupproted;

/*! @brief Boolean value returned from getMerchantCapability. Merchant account supports contactless.  */
@property(nonatomic,) BOOL isContactlessSupported;

/*! @brief Boolean value returned from getMerchantCapability. Merchant account supports quick chip.  */
@property(nonatomic) BOOL isQuickChipSupported;

/*! @brief Set to true to enable more debugging logs from the middleware. Default value is false. */
@property(nonatomic) BOOL enableDebugLog;

/*! @brief Sourcekey obtained in merchant console. */
@property(nonatomic, retain) NSString *sourceKey;

/*! @brief Pin number from sourcekey. Only needed if sourcekey has a pin assigned */
@property(nonatomic, retain) NSString *pinNum;

/*!@brief Returns the current library version */
@property(nonatomic, retain) NSString *libraryVersion;

@end
