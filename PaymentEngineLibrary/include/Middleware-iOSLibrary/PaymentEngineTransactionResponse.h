//
//  TransactionResponse.h
//  Middleware-iOSLibrary
//
//  Created by PaymentEngine on 7/30/15.
//  Copyright (c) 2015 PaymentEngine. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PaymentEngineReceiptInfo.h"

@class PaymentEngineCurrencyAmount;

@interface PaymentEngineTransactionResponse : NSObject

@property (nonatomic, readwrite) NSString *RefNum;
@property (nonatomic, readwrite) uint64_t RestApiRefNum;
@property (readwrite, nonatomic) int BatchRefNum;
@property (readwrite, nonatomic) int BatchNum;
@property (nonatomic, retain) NSString *Result;
@property (nonatomic, retain) NSString *ResultCode;
@property (nonatomic, retain) NSString *AuthCode;
@property (nonatomic, retain) NSString *AvsResultCode;
@property (nonatomic, retain) NSString *AvsResult;
@property (nonatomic, retain) NSString *RemainBalance;
@property (nonatomic, retain) NSString *CardCodeResultCode;
@property (nonatomic, retain) NSString *CardCodeResult;
@property (nonatomic, retain) NSString *CardReadResult; // This is only available for icmp (Card Removed, Card Inserted, Card Read Error, Bad Read)
@property (nonatomic, retain) NSString *ErrorCode;
@property (nonatomic, retain) NSString *Error;
@property (readwrite, nonatomic) int CustNum;
@property (nonatomic, retain) NSString *AcsUrl;
@property (nonatomic, retain) NSString *Payload;
@property (nonatomic, retain) NSString *VpasResultCode;
@property (readwrite, nonatomic) BOOL     isDuplicate;
@property (readwrite, retain) PaymentEngineCurrencyAmount     *ConvertedAmount;
@property (nonatomic, retain) NSString *ConvertedAmountCurrency;
@property (readwrite, retain) PaymentEngineCurrencyAmount     *ConversionRate;
@property (nonatomic, retain) NSString *Status;
@property (nonatomic, retain) NSString *StatusCode;
@property (readwrite, retain) NSString *authAmount;
@property (readwrite, retain) NSString *cashbackAmount;
@property (nonatomic, retain) NSString *iccData;
@property (nonatomic) BOOL isSignatureRequire;
@property (nonatomic, retain) PaymentEngineReceiptInfo *receiptInfo;  // This is only available for icmp
@property (nonatomic, retain) NSString *procRefNum;
@property (nonatomic, retain) NSString *transKey;
@property(nonatomic, retain) NSString *entryMode; 
@property(nonatomic, retain) NSString *maskedCardNum; //Only available in MP200
@property(nonatomic, retain) NSString *cardHolderName; //Only available in MP200
@property(nonatomic, retain) NSString *cardType; //Only available in MP200
@property(nonatomic, retain) NSString *expirationDate; //Only available in MP200
@property(nonatomic, retain) NSString *trackData; //Only available in MP200
@property(nonatomic)  ReturnTransactionType cvmResult; //Only available in MP200
@property(nonatomic, retain) NSString *transactionType; //nfc, swipe, insert, fallback, or manually (MP200 Only)
@property(nonatomic, retain) NSString *identifier; // Returns offline transaction identifier. Empty if transaction is online
@property(nonatomic) Boolean isOffline; // Return true if transaction is offline, false otherwise


-(id) initWithXML:(NSString *)string;


@end
