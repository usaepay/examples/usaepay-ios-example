//
//  PaymentEngineResultQueue.h
//  Middleware-iOSLibrary
//
//  Created by jay lei on 9/4/18.
//  Copyright © 2018 USAePay. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PaymentEngineResultQueue : NSObject

@property(nonatomic, retain) NSString *identifier; // MD5 identifier for last transaction
@property(nonatomic) double totalAmount; // Total transaction amount
@property(nonatomic, retain) NSString *cardNum; // Masked card number
@property(nonatomic, retain) NSDate *dateTime; // Date and time of the queued transaction
@property(nonatomic, retain) NSString *cardType; // The card type of the card (Visa, Master, Discover, Amex)
@property(nonatomic, retain) NSString *cardHolder; // The card holder's full name if available

@end
