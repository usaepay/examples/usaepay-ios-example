//
//  ReceiptInfo.h
//  Middleware-iOSLibrary
//
//  Created by USAePay on 9/2/15.
//  Copyright (c) 2015 PaymentEngine. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PaymentEngineMiddlewareSettings.h"

@class PaymentEngineCurrencyAmount;

@interface PaymentEngineReceiptInfo : NSObject

@property(nonatomic, retain) NSString *invoiceNum;
@property(nonatomic, retain) NSString *transResult;
@property(nonatomic, retain) NSString *refNum;
@property(nonatomic, retain) NSString *transError;
@property(nonatomic, retain) NSString *authCode;
@property(nonatomic, retain) NSString *transAID;
@property(nonatomic, retain) NSString *transAIDName;
@property(nonatomic) int transCounter;
@property(nonatomic, retain) NSString *appCryptogram;
@property(nonatomic, retain) NSString *terminalSerialNum;
@property(nonatomic, retain) NSString *maskedCardNum;
@property(nonatomic, retain) NSString *cardHolderName;
@property(nonatomic, retain) NSString *amount;
@property(nonatomic, retain) NSString *tipAmount;
@property(nonatomic, retain) NSString *taxAmount;
@property(nonatomic, retain) NSString *discountAmount;
@property(nonatomic, retain) NSString *command;
@property(nonatomic, retain) NSString *transResponseCode;
@property(nonatomic, retain) NSString *transDataSource;
@property(nonatomic) ReturnTransactionType cvmResults;
@property(nonatomic, retain) NSString *cardType;

/**
 * If the transaction is approved, the array will be nil
 * If the transaction is declined, the array will contain a list of tag values that needed to be on the receipt
 */
@property(nonatomic,retain) NSArray *declinedArrayTags;
 

@end
